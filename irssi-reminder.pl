# Irssi-urlfinder
#
#
use utf8;
use strict;
use vars qw($VERSION %IRSSI);
use Irssi qw(command_bind signal_add timeout_add command);
use warnings;
use DateTime;
use DateTime::Format::Strptime qw( );
use POSIX;
use TryCatch;

$VERSION = '1.0.0';
%IRSSI   = (
    authors     => 'Antti-Jussi Nieminen',
    contact     => 'ajnieminen@kapsi.fi',
    name        => 'Irssi-reminder',
    description => 'Irssi reminder',
    license     => 'GPL',
);

my $format;
my $timer;
my @temp_array;

sub cmd_reminder {
    my ( $server, $msg, $nick, $address, $target ) = @_;
    $target = lc $target;

    $msg =~ s/ +/ /;    #remove extra whitespaces

    if ( $msg =~ /^!reminder help/ ) {
        print_help_msg( $server, $target );
        return;
    }

    if ( $msg !~ /^!reminder / ) {
        return;
    }

    $msg =~ s/^!reminder //;

    my $dt = parse_remind_time( \$msg );

    $msg =~ s/^\s+|\s+$//g;

    add_reminder( $server, $target, $nick, $msg, $dt );
    send_confirm( $server, $target, $nick, $msg, $dt );
}

sub add_reminder {
    my ( $server, $target, $nick, $msg, $dt ) = @_;
    my %tmp = (
        "server" => $server,
        "target" => $target,
        "nick"   => $nick,
        "msg"    => $msg,
        "dt"     => $dt,
        "delete" => 0
    );
    push( @temp_array, \%tmp );

    # $server->command("MSG $target Title: $data");
}

sub send_confirm {
    my ( $server, $target, $nick, $msg, $dt ) = @_;
    my $dt_str = date_str($dt);
    $server->command("MSG $target lisätty muistutus: $dt_str");
}

sub date_str {
    my ($dt) = @_;
    return $format->format_datetime($dt);
}

# 1h30m, 30m, 2h, 22:30, 02.08 23:00
sub parse_remind_time {
    my ($msg) = @_;
    my $dt  = DateTime->now( time_zone => 'local' );
    my $now = DateTime->now( time_zone => 'local' );
    my $rg_hm = qr/^(\d+)h(\d+)m/;
    my $rg_h  = qr/^(\d+)h/;
    my $rg_m  = qr/^(\d+)m/;
    my $rg_t  = qr/^([01]?[0-9]|2[0-3]):([0-5][0-9])/;
    my $rg_d  = qr/^(0?[1-9]|[12][0-9]|3[0-1])\.(0?[1-9]|1[0-2])/;
    my $rg_d_t =
qr/^(0?[1-9]|[12][0-9]|3[0-1])\.(0?[1-9]|1[0-2]) ([01]?[0-9]|2[0-3]):([0-5][0-9])/;

    if ( $$msg =~ /$rg_hm/ ) {

        # 1h30m
        $$msg =~ s/$rg_hm//;
        $dt->add( hours   => $1 );
        $dt->add( minutes => $2 );
    }
    elsif ( $$msg =~ /$rg_h/ ) {

        # 1h
        $$msg =~ s/$rg_h//;
        $dt->add( hours => $1 );
    }
    elsif ( $$msg =~ /$rg_m/ ) {

        # 30m
        $$msg =~ s/$rg_m//;
        $dt->add( minutes => $1 );
    }
    elsif ( $$msg =~ /$rg_t/ ) {

        # 23:00
        $$msg =~ s/$rg_t//;
        $dt->set_hour($1);
        $dt->set_minute($2);
        my $cmp = DateTime->compare( $dt, $now );
        if ( $cmp < 1 ) {
            $dt->add( days => 1 );
        }
    }
    elsif ( $$msg =~ /$rg_d_t/ ) {

        # 13.4 09:40
        $$msg =~ s/$rg_d_t//;
        my $d  = $1;
        my $mo = $2;
        my $h  = $3;
        my $m  = $4;
        $dt->set_month($mo);
        $dt->set_day($d);
        $dt->set_hour($h);
        $dt->set_minute($m);
        my $cmp = DateTime->compare( $dt, $now );

        if ( $cmp < 1 ) {
            $dt->add( years => 1 );
        }
    }

    return $dt;
}

sub print_help_msg {
    my ( $server, $target ) = @_;
    $server->command("MSG $target !reminder <aika> [<viesti>]");
    $server->command("MSG $target 10m, 1h, 1h20m, 12:30, 14.10 13:00");
    $server->command("MSG $target jos bugaa. syytä Juhoa :)");
}

sub reload_settings {
}

sub timer_command {
    my @del_array = ();
    my $now = DateTime->now( time_zone => 'local' );

    try {
        while ( my ( $i, $el ) = each @temp_array ) {
            my %rem = %{ $temp_array[$i] };
            my $dt  = $rem{"dt"};

            my $cmp = DateTime->compare( $dt, $now );

            if ( $cmp < 1 ) {
                do_remind(%rem);
                push @del_array, $i;
            }

        }

        while ( my ( $i, $el ) = each @del_array ) {
            delete @temp_array[ $del_array[$i] ];
        }
    }
    catch($err) {
        Irssi::print("error: $err");
    };
}

sub do_remind {
    my (%rem)  = @_;
    my $server = $rem{"server"};
    my $target = $rem{"target"};
    my $nick   = $rem{"nick"};
    my $msg    = $rem{"msg"};
    my $dt_str = date_str( $rem{"dt"} );
    $server->command("MSG $target $nick: muistutus ('$msg')");
    $rem{"delete"} = 1;
}

sub init {
    @temp_array = ();
    $timer = timeout_add( 10000, \&timer_command, "" );
    $format = DateTime::Format::Strptime->new( pattern => '%H:%M %d.%m.%Y' );
}

init();

Irssi::signal_add( 'setup changed', 'reload_settings' );
Irssi::signal_add_last( 'message public', 'cmd_reminder' );
