# this script should ease my life by checking updates from openrepos
# and from github.... awesome.
# and thanks to irssi_logger.pl (https://github.com/qbit/irssi_logger)

# sudo cpanm DBI
# sudo cpanm URI::Find

use utf8;
use strict;
use vars qw($VERSION %IRSSI);
use Irssi qw(command_bind signal_add);
use IO::File;
use warnings;
use LWP::Simple;
use DBI;
use URI::URL;
use TryCatch;
use Archive::Extract;
use XML::Simple;
use Data::Dumper;

my $tmp_dir = "/home/ajnieminen/tmp";
my $repo_url = "https://sailfish.openrepos.net/%s/personal/main/repodata/primary.xml.gz";

$VERSION = '0.0.0';
%IRSSI   = (
    authors     => 'Antti-Jussi Nieminen',
    contact     => 'ajnieminen@kapsi.fi',
    name        => 'Irssi-repontify',
    description => 'Irssi openrepos notifier',
    license     => 'GPL',
);

my $db;
my @temp_array;

my $create_table_url = qq~
CREATE TABLE IF NOT EXISTS URL (
    ID INT PRIMARY KEY,
    URL TEXT,
    AUTHOR TEXT,
    NAME TEXT,
    UPDATED TEXT,
    VERSION TEXT,
    DOWNLOAD TEXT
);
~;

my $create_table_nick = qq~
CREATE TABLE IF NOT EXISTS URL_NICK (
    ID INT PRIMARY KEY,
    URL_ID INT,
    NICK TEXT
);
~;

my $last_rowid = qq~
SELECT LAST_INSERT_ROWID();
~;

my $insert_db_url = qq~
INSERT INTO URL VALUES (null, %s, %s, %s, %s, %s, %s);
~;

my $select_id_by_url_from_url = qq~
SELECT ID FROM URL WHERE URL = ?;
~;

sub init_db {
    # create table if not exists TableName (col1 typ1, ..., colN typN)
    $db->do($create_table_url);
    $db->do($create_table_nick);
}

sub connect_db {
    $db = DBI->connect( "dbi:SQLite:dbname=reponotify.db", "", "" );
    init_db();
}

sub parse_site {
    my $url = $_;
    my $uri  = new URI::URL $url;

    if ( $uri->host !~ /^openrepos\.net/ ) {
        return;
    }
}

sub parse_repo {
    my $url = $_;

    if ($url =~  /content\/(.+\/.+)$/) {
        return $1;
    }
}

sub url_callback {
    my ( $uri, $text ) = @_;

    push @temp_array, $text;

    return $text;
}

sub check_for_url {
    my ($msg) = @_;

    @temp_array = ();
    my $finder = URI::Find->new( \&url_callback );
    $finder->find( \$msg );
}

sub get_file {
    my ($repo) = @_;
    my $tmp_url = sprintf($repo_url, $repo);
    my $tmp_file = $tmp_dir . "/" . $repo . "_primary.xml.gz";
    getstore($tmp_url, $tmp_file);
    my $ae = Archive::Extract->new( archive => $tmp_file );
    my $ok = $ae->extract( to => $tmp_dir );
}

sub get_version {
    my ($repo, $app) = @_;

    my $xml = XML::Simple->new;
    my $data = $xml->XMLin($tmp_dir . "/" . $repo . "_primary.xml");

    if (defined $data->{package}{'harbour-' . $app}) {
        return $data->{package}{'harbour-' . $app}{version}{ver};
    }
    return "";
}

sub cmd_publicmsg {
    my ( $server, $msg, $nick, $address, $target ) = @_;

    $msg =~ s/ +/ /;    #remove extra whitespaces

    if ( $msg !~ /^!reponotify / ) {
        return;
    }

    $db = connect_db() unless $db;
    $target = lc $target;

    $msg =~ s/^!reponotify //;
    check_for_url($msg);

    foreach (@temp_array) {
        my $url  = $_;

        my $url_id = get_url_id($url);
        if ($url_id < 0) {
            manage_openrepos($url);
        } else {
            add_url_to_nick();
        }

    }
}

sub manage_openrepos {
    my $url = $_;

    my $repo = parse_repo($url);
    my @repo_r = split('/', $repo);
    get_file($repo_r[0]);
    my $version = get_version($repo_r[0], $repo_r[1]);
    Irssi::print($version);
}

sub add_url_to_nick {

}

sub reload_settings {
}

sub get_url_id {

}

Irssi::signal_add( 'setup changed', 'reload_settings' );
Irssi::signal_add_last( 'message public', 'cmd_publicmsg' );
