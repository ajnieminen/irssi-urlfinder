use utf8;
use strict;
use vars qw($VERSION %IRSSI);
use Irssi qw(command_bind signal_add);
use IO::File;
use warnings;
use LWP::Simple;
use WWW::Telegram::BotAPI;
use Data::Dumper;

$VERSION = '1.0.0';
%IRSSI   = (
    authors     => 'Antti-Jussi Nieminen',
    contact     => 'ajnieminen@kapsi.fi',
    name        => 'Telegram bot',
    description => 'Telegram bot',
    license     => 'GPL',
);

my $api;
my $chatId;
my $updateId;
my $hilights = "balthamaisteri antti-jussi";
my @hl = split(" ", $hilights);
my $my_server;
my $my_target;

# these are required
my $token   = "";
my $path    = "";
my $gpath   = "";
my $channel = "";

sub init_api {
  $api = WWW::Telegram::BotAPI->new (
    token => $token
  );
}

sub init_chat_id {
  my $val = $api->getUpdates;
  foreach my $n (@{$val->{result}}) {
    my $title = $n->{message}->{chat}->{title};
    if (!defined $chatId && $title eq $channel)
    {
      $chatId = $n->{message}->{chat}->{id};
      last;
    }
  }
}

sub send_chat_message {
  my ( $nick, $target, $msg ) = @_;
  init_api() unless $api;
  init_chat_id() unless $chatId;

  if (defined $chatId) {
    $api->sendMessage ({
      chat_id => $chatId,
      text    => join "", $nick, "@", $target, ": ", $msg
    });
  }
}

sub cmd_telebot {
  my ( $server, $msg, $nick, $address, $target ) = @_;
  $target = lc $target;

  # set target for later
  if ($channel eq $target)
  {
    if (!defined $my_server) {
      $my_server = $server;
      $my_target = $target;
    }
  } else {
    return 0;
  }

  # is there a hilight
  foreach my $n (@hl) {
    if (index($msg, $n) != -1) {
      init_api() unless $api;
      init_chat_id() unless $chatId;
      send_chat_message($nick, $target, $msg);
      last;
    }
  }
}

Irssi::settings_add_str( "telebot", "whitelist_channels", "" );
Irssi::signal_add_last( 'message public', 'cmd_telebot' );
