irssi-urlfinder
===============

This scripts listens urls, and finds out site title and saves it for later.

You need some required perl-libraries, but surely you can find em? Some was
available from ubuntu repositories. You must also install mongodb if you want
to save found urls. If you dont want to save urls, you can disable save
option.

This scrips also check given url, and if any useful information is given, it
echoes them on the channge url was found on. This functionality can also be
disabled.

Settings:

blacklist_channels : List of channels irssi-urlfinder is disabled.
whitelist_channels : Only these channels are enabled, others are disabled.
blacklist_users    : List of users that are ignored.
whitelist_users    : Only these users are listened.
show_url_data      : true / false
save_to_db         : true / false
