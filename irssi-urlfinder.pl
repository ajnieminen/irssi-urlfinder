# Irssi-urlfinder
#
# This scripts listens urls, and finds out site title and saves it for later.
#
# You need some required perl-libraries, but surely you can find em? Some was
# available from ubuntu repositories. You must also install mongodb if you want
# to save found urls. If you dont want to save urls, you can disable save
# option.
#
# This scrips also check given url, and if any useful information is given, it
# echoes them on the channge url was found on. This functionality can also be
# disabled.
#
# MongoDB: http://docs.mongodb.org/ecosystem/drivers/perl/
# Also, cpanminus is your friend. Arch Linux:
# sudo cpanm MongoDB
# sudo cpanm LWP
# sudo cpanm URI::Find
# sudo cpanm DateTime
# sudo cpanm LWP::Protocol::https
# sudo cpanm JSON
# Settings:
#
#
# blacklist_channels : List of channels irssi-urlfinder is disabled.
# whitelist_channels : Only these channels are enabled, others are disabled.
# blacklist_users    : List of users that are ignored.
# whitelist_users    : Only these users are listened.
# show_url_data      : true / false
# save_to_db         : true / false
#
use utf8;
use strict;
use vars qw($VERSION %IRSSI);
use Irssi qw(command_bind signal_add);
use IO::File;
use warnings;

$VERSION = '1.0.0';
%IRSSI   = (
    authors     => 'Antti-Jussi Nieminen',
    contact     => 'ajnieminen@kapsi.fi',
    name        => 'Irssi-urlfinder',
    description => 'Irssi url listener',
    license     => 'GPL',
);

use LWP::UserAgent;
use HTML::HeadParser;
use URI::Find;
use HTML::TokeParser;
use URI::URL;
use DateTime;
use JSON;

my $blacklist_channels = "";
my $whitelist_channels = "";
my $blacklist_users    = "";
my $whitelist_users    = "";
my $show_url_title     = "true";
my $save_to_db         = "true";
my $debugging          = "false";
my $search_reddit      = "true";
my $do_tiny_url        = "true";

my @temp_array;

sub do_reddit_search {
    my ( $ua, $url ) = @_;
    my $redditUrl = "";

    my $response =
      $ua->get( "https://www.reddit.com/api/info.json?url=" . $url );

    my $json      = JSON->new;
    my $data      = $json->decode( $response->content );
    my $permlink  = $data->{data}->{children}->[0]->{data}->{id};
    my $subreddit = $data->{data}->{children}->[0]->{data}->{subreddit};
    if ($permlink) {
        $redditUrl = "http://redd.it/" . $permlink . " - r/" . $subreddit;
    }
    return $redditUrl;
}

sub save_to_db {

    # turns out that mongo was not that fun
    # this will be removed someday... sry
}

sub write_to_file {
    my ( $nick, $channel, $url, $data ) = @_;

    my $now = DateTime->now( time_zone => 'local' );

    my $filename = '/home/ajnieminen/urls.txt';
    open( my $fh, '>>', $filename ) or die "Tiedostoa ei saada auki";

    say $fh "$now - $channel - $nick - $url - $data";
    close $fh;
}

sub check_channel {
    my ($channel) = @_;

    if ( !defined $whitelist_channels and !defined $blacklist_channels ) {
        return 1;
    }

    my @whitechannels = split( ' ', $whitelist_channels );
    my @blackchannels = split( ' ', $blacklist_channels );

    if (@whitechannels) {
        my %hash = map { $_ => 1 } @whitechannels;
        if ( exists( $hash{$channel} ) ) {
            return 1;
        }
    }
    elsif (@blackchannels) {
        my %hash = map { $_ => 1 } @blackchannels;
        if ( exists( $hash{$channel} ) ) {
            return 0;
        }
    }

    return 1;
}

sub check_user {
    my ($user) = @_;

    if ( !defined $whitelist_users and !defined $blacklist_users ) {
        return 1;
    }

    my @whiteusers = split( ' ', $whitelist_users );
    my @blackusers = split( ' ', $blacklist_users );

    if (@whiteusers) {
        my %hash = map { $_ => 1 } @whiteusers;
        if ( exists( $hash{$user} ) ) {
            return 1;
        }
    }
    elsif (@blackusers) {
        my %hash = map { $_ => 1 } @blackusers;
        if ( exists( $hash{$user} ) ) {
            return 0;
        }
    }

    return 1;
}

sub parse_site {
    my ($url) = @_;

    my $data = "";
    my $uri  = new URI::URL $url;

    if ( $uri->host =~ /^(www\.)*tinyurl\.com/ ) {
        $url = 'http://preview.tinyurl.com' . $uri->path;
    }
    elsif ( $uri->host =~ /^(i\.)*imgur\.com/ ) {
        my $tmpPath = $uri->path;
        $tmpPath =~ s/\..*//;
        $url = 'http://' . $uri->host . $tmpPath;
    }
    elsif ( $uri->host =~ /(www\.)*facebook\.com/ ) {
        return "";
    }

    my $ua       = LWP::UserAgent->new;
    my $response = $ua->get($url);

    if ( $debugging eq "true" ) {
        Irssi::print("response: ");
        Irssi::print( $response->content );
    }

    if ( $response->is_success ) {

        my $p = HTML::HeadParser->new;
        $p->parse( $response->content );

        if ( $debugging eq "true" ) {
            Irssi::print("headparser data: ");
            Irssi::print($p);
        }

        $data = $p->header('Title');

        if (   $uri->host =~ /^(www\.)*youtube\.com/
            || $uri->host =~ /^youtu\.be/ )
        {
            if ( $response->content =~ /P(\d+)*T(\d+)*M(\d+)*S/ ) {
                my $t = $1;
                my $m = $2;
                my $s = $3;

                $data .= " (";

                if ($t) {
                    $data .= "$t" . "t ";
                }
                if ($m) {
                    $data .= "$m" . "m ";
                }
                if ($s) {
                    $data .= "$s" . "s";
                }

                $data .= ") ";
            }
        }

        if ( $uri->host =~ /^(www\.)*tinyurl\.com/ ) {
            my $start = 'id="redirecturl" href="';
            my $end   = '"';

            if ( $response->content =~ /$start(.*?)$end/ ) {
                $data = "TinyUrl. Preview: " . $1;
            }
        }

        if (   $search_reddit eq "true"
            && $uri->host !~ /(i\.)*imgur\.com/
            && $uri->host !~ /(www\.)*reddit\.com/
            && $uri->host !~ /(www\.)*tinyurl\.com/ )
        {
            my $link = do_reddit_search( $ua, $url );
            if ( $link ne "" ) {
                $data .= " (reddit: " . $link . ")";
            }

            if ( $debugging eq "true" ) {
                Irssi::print( "reddit link: " . $link );
            }

        }
    }

    return $data;
}

sub url_callback {
    my ( $uri, $text ) = @_;

    push @temp_array, $text;

    return $text;
}

sub check_for_url {
    my ($msg) = @_;

    @temp_array = ();
    my $finder = URI::Find->new( \&url_callback );
    $finder->find( \$msg );
}

sub cmd_urlsniffer {
    my ( $server, $msg, $nick, $address, $target ) = @_;
    $target = lc $target;

    if ( !check_channel($target) ) {
        return 0;
    }

    if ( !check_user($nick) ) {
        return 0;
    }

    check_for_url($msg);

    foreach (@temp_array) {
        my $url  = $_;
        my $data = parse_site($url);

        if ( $debugging eq "true" ) {
            Irssi::print( "parsed site data: '" . $data . "'" );
        }

        if ( $show_url_title eq "true" && $data !~ /^\s*$/ ) {
            $server->command("MSG $target $data");
        }
        if ( $save_to_db eq "true" ) {
            save_to_db( $nick, $target, $url, $data );
            write_to_file( $nick, $target, $url, $data );
        }
    }
}

sub reload_settings {
    $blacklist_channels = Irssi::settings_get_str('blacklist_channels');
    $whitelist_channels = Irssi::settings_get_str('whitelist_channels');
    $blacklist_users    = Irssi::settings_get_str('blacklist_users');
    $blacklist_users    = Irssi::settings_get_str('whitelist_users');
    $show_url_title     = Irssi::settings_get_str('show_url_data');
    $save_to_db         = Irssi::settings_get_str('save_to_db');
    $debugging          = Irssi::settings_get_str('debugging');
}

Irssi::settings_add_str( "urlsniffer", "blacklist_channels", "" );
Irssi::settings_add_str( "urlsniffer", "whitelist_channels", "" );
Irssi::settings_add_str( "urlsniffer", "blacklist_users",    "" );
Irssi::settings_add_str( "urlsniffer", "whitelist_users",    "" );
Irssi::settings_add_str( "urlsniffer", "show_url_data",      "true" );
Irssi::settings_add_str( "urlsniffer", "save_to_db",         "true" );
Irssi::settings_add_str( "urlsniffer", "debugging",          "false" );
Irssi::settings_add_str( "urlsniffer", "search_reddit",      "true" );
Irssi::settings_add_str( "urlsniffer", "do_tiny_url",        "true" );

Irssi::signal_add( 'setup changed', 'reload_settings' );
Irssi::signal_add_last( 'message public', 'cmd_urlsniffer' );
